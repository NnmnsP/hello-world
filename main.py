def user_input(prompt):
    # the input function will display a message in the terminal
    # and wait for user input.
    user_input = input(prompt)
    return user_input

name = user_input("what's your name? ")
age = int(user_input("how old are you? "))

print("Hello world")
print("Congratulations", name)
print("You have lived for", age, "years")
print("Goodbye")
